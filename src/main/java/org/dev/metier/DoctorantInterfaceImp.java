package org.dev.metier;

import java.util.List;

import org.dev.dao.DoctorantRepository;
import org.dev.entities.Doctorant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.QPageRequest;
import org.springframework.stereotype.Service;


@Service
public class DoctorantInterfaceImp implements doctorantInterface {
	
	@Autowired
	DoctorantRepository doctorantRepository;
	public Page<Doctorant> listedoctorants(String motCle,int page, int size) {
	Pageable pageable=PageRequest.of(page, size);
	return doctorantRepository.chercher(motCle,pageable);
	}
	
	@Override
	public Doctorant save(Doctorant d) {
	Doctorant doctorant=doctorantRepository.save(d);
	return doctorant;
	}	
}

