package org.dev.metier;

import java.util.List;

import org.dev.entities.Doctorant;
import org.springframework.data.domain.Page;

public interface doctorantInterface {

	public Page<Doctorant> listedoctorants(String motCle,int page, int size);
	public Doctorant save(Doctorant d);
}
