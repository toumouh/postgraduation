package org.dev.web;

import java.util.List;

import org.dev.entities.Doctorant;
import org.dev.metier.doctorantInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@CrossOrigin(origins="*",allowedHeaders="*")
public class DoctorantControllerRest {
	@Autowired
	doctorantInterface doctorantinterface;
	
	@CrossOrigin("*")
	@GetMapping(value="/ListeDoctorants")
	public Page<Doctorant> index(Model model, 
		@RequestParam (name="motCle", defaultValue="")	String motCle,
		@RequestParam (name="page",defaultValue="0") int p, 
		@RequestParam (name="size",defaultValue="4") int s) {
		Page<Doctorant> pagedoctorant= doctorantinterface.listedoctorants("%"+motCle+"%",p, s);
	    return pagedoctorant;
	}
	
	@CrossOrigin("*")
	@PostMapping(value="/saveDoctorantRest")
	public String saveDoctorant(@RequestBody   Doctorant d) {
	Doctorant doctorant=doctorantinterface.save(d);
	return "Inscription terminée";
	}
	
}
