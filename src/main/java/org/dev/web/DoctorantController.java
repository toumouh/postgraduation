package org.dev.web;

import java.util.List;

import org.dev.entities.Doctorant;
//import org.dev.metier.DoctorantInterfaceImp;
import org.dev.metier.doctorantInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class DoctorantController {
	@Autowired
	doctorantInterface doctorantinterface;
	
	@GetMapping(value="/doctorants")
	public String index(Model model, 
		@RequestParam (name="motCle", defaultValue="")	String motCle,
		@RequestParam (name="page",defaultValue="0") int p, 
		@RequestParam (name="size",defaultValue="4") int s) {
		Page<Doctorant> pagedoctorant= doctorantinterface.listedoctorants("%"+motCle+"%",p, s);
		List<Doctorant> doctorants= pagedoctorant.getContent();
		int [] pages = new int[pagedoctorant.getTotalPages()];
		model.addAttribute("listDoctorants",doctorants);
		model.addAttribute("pages",pages);
		model.addAttribute("pagecourante",p);
	    return "doctorants";
	}
	
	@GetMapping(value="/formecomplete")
	public String form(Model model) {
		model.addAttribute("doctorant",new Doctorant());
		return "formecomplete";
	}

	@PostMapping(value="/saveDoctorant")
	public String saveDoctorant(Model model, Doctorant d) {
		Doctorant doctorant=doctorantinterface.save(d);
		//model.addAttribute("doctorant",new Doctorant());
		return "redirect:/doctorants";
	}
	
	@GetMapping(value="/Encadrants")
	public String Enadrant(Model model, Doctorant d) {
		return "/Encadrant";
	}
	
	
	
}
