package org.dev;

import org.dev.dao.DoctorantRepository;
import org.dev.entities.Doctorant;
import org.dev.metier.DoctorantInterfaceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class PostgraduationApplication implements CommandLineRunner {

	@Autowired
	DoctorantRepository doctorantRepository;
	@Autowired
	DoctorantInterfaceImp doctorantInterfaceImp;
	public static void main(String[] args) {
		SpringApplication.run(PostgraduationApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		doctorantRepository.save (new Doctorant());
		doctorantRepository.save (new Doctorant());
		doctorantRepository.save (new Doctorant());
		doctorantRepository.save (new Doctorant());
		doctorantRepository.save (new Doctorant());
		doctorantRepository.save (new Doctorant());
		doctorantRepository.save (new Doctorant());
		doctorantRepository.save (new Doctorant());
		doctorantRepository.save (new Doctorant());
		doctorantRepository.save (new Doctorant());
		doctorantRepository.save (new Doctorant());
		
	}

	/*@Bean
	public WebMvcConfigurer Corconfigurer() {
		
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings (CorsRegistry registry) {
				
				registry.addMapping("/**")
				.allowedMethods("GET","POST","PUT","DELETE")
				.allowedHeaders("*")
				.allowedOrigins("*");
			}
			
		};
		
	}*/
	
}
