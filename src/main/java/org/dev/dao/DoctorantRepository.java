package org.dev.dao;


import org.dev.entities.Doctorant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface DoctorantRepository extends JpaRepository <Doctorant,Long> {
	@Query (value="select  d from Doctorant d where d.nomFr like :x ")
	public Page<Doctorant> chercher(@Param("x")String motCle, Pageable pageable);

}


