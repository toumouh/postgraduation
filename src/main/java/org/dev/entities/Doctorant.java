package org.dev.entities;



import java.util.Date;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
@Entity
@Data @AllArgsConstructor @NoArgsConstructor @ToString

public class Doctorant {
	
	@Id @GeneratedValue
	private Long code;//
	private String nomFr;//
	private String nomAr;//
	private String prenomFr;//
	private String prenomAr;//
	private String prenomPereFr;//
	private String nomMereFr;//
	private String prenomMereFr;//
	
	private String dateNaissance;//
	private String adresse;//
	private String lieuxNaissance;//
	private String wilayaNaissance;//
	private String paysNaissance;//
	private String nationalite;//
	private String tel;//
	private String mail;//
	private String enseignatUniv;//
	private String lieuxTravail;//
	
	private String Diplome1;//
	private Date datediplome1;//
	private String lieuxdiplome1;
	private String Diplome2;//
	private Date datediplome2;//
	private String lieuxdiplome2;
	private String Diplome3;//
	private Date datediplome3;//
	private String lieuxdiplome3;


	private String nomEncadrant;//
	private String prenomEncadrant;//
	private String gradeEncadrant;//
	private String faculteEncadrant; //
	private String etablissementEncadrant;//
	
	private String nomCoencadrant;//
	private String prenomcoencadrant;//
	private String gradecoencadrant;//
	private String facultecoencadrant;//
	private String etablissementcoencadrant;//

	private String sujet;//
	
	private String resume;//
	private String motsCles;//
	
	private String departement;//
	private String specialite;//
	private String ooption;//
	
	private String domaine;
	private String filiere;
	
	
}
